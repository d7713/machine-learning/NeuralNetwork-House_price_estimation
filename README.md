# House price estimation (Neural Network)

### By: Andrew Wairegi

## Description
To create an estimate for the buying price of houses. This will allow realtors to determine the price
of a house using a model. This will reduce the amount of time they need to take when evaluating a house.
Probably reducing the costs as well. Cause they don't have to hire many realtors. This is the purpose of
this notebook.

## Setup/installation instructions
1. Create a local folder on your computer
2. Set it up as an empty repository (using git init)
3. Clone this repository there (git clone https://...)
4. Upload the notebook to google drive
5. Open it
6. Upload the data file to google collab (file upload section)
7. Run the notebook

## Known Bugs
There are no known issues / bugs.

## Technologies Used
1. Python - The programming language
2. Numpy - An Arithmetic Package
3. Pandas - A Data Analysis Package
4. Seaborn - A Visualization package
5. Matplotlib - A Visualization package
6. Scikit learn - A Modelling package

<br>

### License
Copyright © 2021 **Andrew Wairegi**
<br>
You are free to view, but not copy.

[Open notebook]: https://gitlab.com/d7713/machine-learning/NeuralNetwork-House_price_estimation/-/blob/main/House%20pricing%20(Neural%20network).ipynb
